; DESCRIPTION
;        The  strcmp() function compares the two strings s1 and s2.  The locale is not taken into account (for a locale-aware comparison, see strcoll(3)).  The
;        comparison is done using unsigned characters.

;        strcmp() returns an integer indicating the result of the comparison, as follows:

;        • 0, if the s1 and s2 are equal;

;        • a negative value if s1 is less than s2;

;        • a positive value if s1 is greater than s2.

;        The strncmp() function is similar, except it compares only the first (at most) n bytes of s1 and s2.

; RETURN VALUE
;        The strcmp() and strncmp() functions return an integer less than, equal to, or greater than zero if s1 (or the first n bytes thereof)  is  found,  re‐
;        spectively, to be less than, to match, or be greater than s2.

; int strcmp(const char *s1, const char *s2);
; s1 - rdi
; s2 - rsi

		global	ft_strcmp
		section	.text

ft_strcmp:
		mov		rcx, 0					; init counter
		mov		r8, 0					; init register. r8 - 64-bit long, r8b - 8-bit char
		mov		r9, 0					; init register. r9 - 64-bit long, r9b - 8-bit char
		mov		rax, 0					; init return register
		jmp		.compare					; start the loop

.compare:
		mov		r8b, byte [rdi + rcx]	; placing char from current index of s1 to the 8-bit register
		mov		r9b, byte [rsi + rcx]	; same for s2
		cmp		r8b, 0		; check for the end of the string s1
		je		.exit
		cmp		r9b, 0		; check for the end of the string s2
		je		.exit
		cmp		r8b, r9b				; compare current characters in the stings
		jne		.exit
		inc		rcx						; increase counter
		jmp		.compare

.exit:
		sub		r8, r9					; subtract one string
		mov		rax, r8
		ret
