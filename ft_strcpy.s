; The  strcpy() function copies the string pointed to by src, including the terminating null byte ('\0'),
; to the buffer pointed to by dest.  The strings may not overlap,
; and the destination string dest must be large enough to receive the copy.
; Beware of buffer overruns!  (See BUGS.)

;RETURN VALUE
;  The strcpy() and strncpy() functions return a pointer to the destination string dest.

; char *strcpy(char *dest, const char *src);
; dest	- rdi
; src	- rsi

		global	ft_strcpy
		section	.text

ft_strcpy:
		mov		rdx, 0		; dest pointer TODO replace with 8 byte register?
		mov		rcx, 0		; counter
		jmp		.copy

.copy:
		mov		r8b, [rsi + rcx]	; record char on current index to the 8-bit register
		mov		[rdi + rcx], r8b	; move recorded char to the effective address of same index in dest string
		cmp		r8b, 0				; check for the end of the sting
		jne		.strkeep				; continue loop while 0 isn't met
		jmp		.exit

.strkeep:
		inc		rcx
		jmp		.copy

.exit:
		mov		rax, rdi			; return address of the string's start
		ret
