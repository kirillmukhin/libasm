		extern	__errno_location ; function shall return the address of the errno variable for the current thread.

		global	ft_write
		section	.text

ft_write:
		mov		rax, 1	; 1 - rax number for the 'write' syscall in Linux; mac - 0x20000004
		syscall
		cmp		rax, 0	; check the return value of 'write' syscall
		jl		.error	; jl - jump if lower. A -1 return value indicates an error of syscall
		ret				; Return from the ft_write

.error:
		neg		rax					; ???
		push	rax					; save error number to stack
		pop		rdi					; error number now in rdi
		call	__errno_location	; calling a function that will put address of errno variable into rax
		mov		[rax], rdi			; record error number to the address of the errno
		mov		rax, -1				; return value is -1
		ret
