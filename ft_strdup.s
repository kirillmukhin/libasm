; The  strdup() function returns a pointer to a new string which is a duplicate of the string s.  Memory for the new string
;        is obtained with malloc(3), and can be freed with free(3).

; RETURN VALUE
;        On success, the strdup() function returns a pointer to the duplicated string.  It returns NULL if insufficient memory was
;        available, with errno set to indicate the cause of the error.

; char *strdup(const char *s);

;	s -> rdi

		; extern	__errno_location ; function shall return the address of the errno variable for the current thread.
		extern	ft_strlen
		extern	malloc
		extern	ft_strcpy

		global	ft_strdup
		section	.text

ft_strdup:
		cmp		rdi, 0			; check that there's a string
		je		.error
		call	ft_strlen		; rax now has length of the string
		inc		rax				; strlen + 1 for end of the string
		push	rdi				; put current argumet to the stack
		mov		rdi, rax		; mov total len for malloc into the argument's register
		call	malloc
		cmp		rax, 0			; check if malloc failed
		pop		rdi				; bring initial argument (string) back ; pop before error call - is that legal?
		je		.error

		mov		rsi, rdi		; string is now second argument
		mov		rdi, rax		; first argument is now a pointer to the allocated space
		call	ft_strcpy
		ret

.error:
		mov		rax, 0
		ret
