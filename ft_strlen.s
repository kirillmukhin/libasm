; The strlen() function calculates the length of the string pointed to by s, excluding the terminating null byte ('\0').
; RETURN VALUE
;        The strlen() function returns the number of bytes in the string pointed to by s.
; size_t strlen(const char *s);
; s - rdi

		global	ft_strlen
		section	.text

ft_strlen:
		mov		rcx, 0	; counter
		mov		rax, 0	; return
		jmp		.calc	; start loop

.calc:
		cmp		byte [rdi + rcx], 0
		je		.exit			; exit loop when end of the string is reached
		inc		rcx				; increase counter
		jmp		.calc			; continue loop

.exit:
		mov		rax, rcx		; record final index to the accumulator (keep in mind off-by-one)
		ret
