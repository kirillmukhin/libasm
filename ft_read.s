		extern	__errno_location ; function shall return the address of the errno variable for the current thread.

		global	ft_read
		section	.text

ft_read:
		mov		rax, 0	;  0 - rax number for the 'read' syscall in Linux; mac: 0x20000003
		syscall
		cmp		rax, 0	; check the return value of 'read' syscall
		jl		.error	; jl - jump if lower. A -1 return value indicates an error of syscall
		ret				; Return from the ft_read

.error:
		neg		rax					; multiply error number by -1 to make the number positive
		push	rax					; save error number to stack
		pop		rdi					; error number now in rdi
		call	__errno_location	; calling a function that will put address of errno variable into rax
		mov		[rax], rdi			; record error number to the address of the errno
		mov		rax, -1				; return value is -1
		ret
