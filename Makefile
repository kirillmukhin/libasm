NAME = libasm.a

SRCS =	ft_read.s \
		ft_strcmp.s \
		ft_strcpy.s \
		ft_strdup.s \
		ft_strlen.s \
		ft_write.s \

OBJS = $(SRCS:%.s=%.o)

SRC_C = main.c
CC = gcc
CFLAGS = -Wall -Wextra -Werror

.PHONY: all re clean fclean test

all: $(NAME)

$(NAME): $(OBJS)
	ar rc $(NAME) $(OBJS)
	# ranlib $(NAME)
	# warning: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib: warning for library: libasm.a the table of contents is empty (no object file members in the library define global symbols)

%.o: %.s
	nasm -f elf64 -o $@ $<

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all

test: $(NAME)
	# -no-pie option is Linux-only
	# $(CC) $(CFLAGS) $(SRC_C) -L. -lasm -o test
	$(CC) $(CFLAGS) $(SRC_C) -L. -lasm -o test -no-pie

testclean: fclean
	rm -f test

retest: testclean test
