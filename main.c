/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.21-school.ru>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 04:20:00 by jarnolf           #+#    #+#             */
/*   Updated: 2020/12/28 21:45:18 by jarnolf          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm.h"
//#include <sys/types.h> //ssize_t
// #include <unistd.h> //write
// #include <fcntl.h> // open
// #include <stdio.h> //printf
// #include <string.h> //strlen
// #include <strings.h> //bzero
// #include <stdlib.h> //free
// #include <errno.h> //errno, duh

typedef struct	s_write
{
	char	*buff;
	int		fd;
	int		errno_mem;
	size_t	ret;
}				t_write;

int		extra_read(const char *str, int size, t_write *ftog, int og)
{
	char	buff[size];

	errno = 0;
	ftog->buff = buff;
	ftog->errno_mem = 0;
	ftog->fd = open(str, O_RDONLY);
	if (ftog->fd < 0)
		return (0);
	memset(ftog->buff, 'F', size);
	if (og == 0)
		ftog->ret = ft_read(ftog->fd, ftog->buff, size);
	else
		ftog->ret = read(ftog->fd, ftog->buff, size);
	ftog->errno_mem = errno;
	return (1);
}

void	check_read(const char *str, int size)
{
	t_write	ft;
	t_write	og;
	int		open_error;

	open_error = 0;
	if (extra_read(str, size, &ft, 0) == 0)
		open_error = 1;
	if (extra_read(str, size, &og, 1) == 0)
		open_error = 1;
	if ((ft.ret == og.ret) && (ft.errno_mem == og.errno_mem) && (memcmp(ft.buff, og.buff, size) == 0))
	{
		printf("\t[OK] '%s'\n", str);
		if (open_error == 1)
		{
			printf("\t\t[OPEN ERROR]\n");
			return ;
		}
		printf("\t\tret: ft=%lu; og=%lu\n", ft.ret, og.ret);
		printf("\t\terrno: ft=%d; og=%d\n", ft.errno_mem, og.errno_mem);
		printf("\t\tbuff: ft='%s'; og='%s'\n", ft.buff, og.buff);
	}
	else
	{
		printf("\t[ERROR] '%s'\n", str);
		if (open_error == 1)
		{
			printf("\t\t[OPEN ERROR]\n");
		}
		printf("\t\tret: ft=%lu; og=%lu\n", ft.ret, og.ret);
		printf("\t\terrno: ft=%d; og=%d\n", ft.errno_mem, og.errno_mem);
		printf("\t\tbuff: ft='%s'; og='%s'\n", ft.buff, og.buff);
	}
}

void	test_read()
{
	check_read("main.c", 64);
	check_read("libasm.h", 18);
	check_read("", 8);
	check_read("nono", 64);
}

void	check_strcmp(const char *s1, const char *s2)
{
	int	ft;
	int	og;

	ft = ft_strcmp(s1, s2);
	og = strcmp(s1, s2);
	if (ft != og)
		printf("\t[ERROR] '%s' '%s'\n\t\tft_strcmp: %d\n\t\tstrmp: %d\n", s1, s2, ft, og);
	else
		printf("\t[OK] '%s' '%s'\n", s1, s2);
}

void	test_strcmp()
{
	check_strcmp("same", "same");
	check_strcmp("same", "sike");
	check_strcmp("", "empty");
}

void	check_strcpy(const char *str, int size)
{
	char	ft[size];
	char	og[size];

	if (strlen(str) >= (size_t)size)
	{
		printf("\t[TEST ERROR] buffer is to small\n");
		return ;
	}
	bzero(ft, size);
	bzero(og, size);
	ft_strcpy(ft, str);
	strcpy(og, str);
	if ((memcmp(ft, og, size)) != 0)
		printf("\t[ERROR] '%s'[%d]\n", str, size);
	else
		printf("\t[OK] '%s'[%d]\n", str, size);
}

void	test_strcpy()
{
	check_strcpy("Hello there!", 18);
	check_strcpy("Did you ever hear the Tragedy of Darth Plagueis the wise? I thought not. It's not a story the Jedi would tell you.", 128);
	check_strcpy("Now this is podracing!", 32);
}

void	check_strdup(const char *str)
{
	char	*ptr_ft;
	char	*ptr_og;
	size_t	len;

	ptr_ft = ft_strdup(str);
	if (ptr_ft == NULL)
	{
		printf("\t[MALLOC ERROR] %s", str);
		return ;
	}
	ptr_og = strdup(str);
	if (ptr_og == NULL)
	{
		printf("\t[MALLOC ERROR] %s", str);
		free(ptr_ft);
		return ;
	}
	len = strlen(ptr_og);
	if ((memcmp(ptr_ft, ptr_og, len)) != 0)
		printf("\t[ERROR] %s\n", str);
	else
		printf("\t[OK] %s\n", str);
	free(ptr_ft);
	free(ptr_og);
}

void	test_strdup()
{
	check_strdup("");
	check_strdup("Pootis");
	check_strdup("Words cannot express how much I hate France right now!");
}

void	check_strlen(const char *str)
{
	size_t	ft;
	size_t	og;

	ft = ft_strlen(str);
	og = strlen(str);
	if (ft != og)
		printf("\t[ERROR] '%s'\n\t\tft_strlen(%zd)\n\t\tstrlen(%zd)\n", str, ft, og);
	else
		printf("\t[OK] '%s'\n", str);
}

void	test_strlen()
{
	check_strlen("");
	check_strlen("1");
	check_strlen("Hello there!");
}

void	check_write(int fd, const char *str)
{
	size_t	count;
	size_t	ret_ft;
	size_t	ret_og;
	int		errno_ft;
	int		errno_og;

	count = strlen(str);

	errno = 0;
	printf("\tft_write(%d, \"%s\", %lu):\n\t", fd, str, count);
	ret_ft = ft_write(fd, str, count);
	errno_ft = errno;
	printf("\n");

	errno = 0;
	printf("\twrite(%d, \"%s\", %lu):\n\t", fd, str, count);
	ret_og = write(fd, str, count);
	errno_og = errno;
	printf("\n");


	if ((errno_ft == errno_og) && (ret_ft == ret_og))
		printf("\n\t[OK]\n");
	else
	{
		printf("\n\t[ERROR]\n");
		printf("\t\terrno: ft:%d, og:%d\n", errno_ft, errno_og);
		printf("\t\tret: ft:%lu, og:%lu\n", ret_ft, ret_og);
	}
}

void	test_write()
{
	check_write(1, "Pootis!");
}

int		main(void)
{
	printf("ft_read:\n");
	test_read();
	printf("ft_strcmp:\n");
	test_strcmp();
	printf("ft_strcpy:\n");
	test_strcpy();
	printf("ft_strdup:\n");
	test_strdup();
	printf("ft_strlen:\n");
	test_strlen();
	printf("ft_write:\n");
	test_write();
}
